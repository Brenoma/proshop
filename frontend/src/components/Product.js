import React from 'react';
import { Card } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Rating from './Rating';
import { Link } from 'react-router-dom';
const Product = ({ product }) => {
    return (
        <Card className="my-3 p-3 rounded">
            <Link to={`/product/${product._id}`}>
                <Card.Img src={product.image} variant="top" />
            </Link>
            <Card.Body>
                <Link to={`/product/${product._id}`}>
                    <Card.Title>
                        <strong>{product.name}</strong>
                    </Card.Title>

                    <Card.Text as="div">
                        <Rating
                            value={product.rating}
                            text={`${product.numReviews} reviews`}
                        />
                    </Card.Text>

                    <Card.Text as="h3">${product.price}</Card.Text>
                </Link>
            </Card.Body>
        </Card>
    );
};
Rating.propTypes = {
    value: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    color: PropTypes.string,
};
export default Product;
